﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NestedVoid
{
    class Program
    {
        static void Main(string[] args)
        {
            Testing();
        }

        static void Testing()
        {
            double diameter = 10;
            double area = area_calculator(diameter);

            double area_calculator(double diameterValue1)
            {
                double radius = radius_calculator(diameterValue1);

                double radius_calculator (double diameterValue2)
                {
                    return diameterValue2 / 2;
                }
                return radius * radius * 3.14f;
            }
            Console.WriteLine(area);
            Console.Write("\nProcess done...");
            Console.ReadKey();
        }
    }
}
