﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternMatching
{
    class Program
    {
        static void Main(string[] args)
        {
            int input_int = 10;
            string input_string = "Hello";

            PrintOut(input_int);
            PrintOut(input_string);
            Console.Write("\nProcess done...");
            Console.ReadKey();
        }

        static void PrintOut(object inputObject)
        {
            if (inputObject is int)
            {
                Console.WriteLine($"Input is an integer: {inputObject}");
            }
            else if (inputObject is string)
            {
                Console.WriteLine($"Input is a string: {inputObject}");
            }
            else return;
        }
    }
}
